# Paste Printer Kit

![](img/DSC09098.jpg)

The story TBD
--
The system will work with air pressure for the feeding system (safer, easier to set up and cleaner) and a motorised controlled extruder head.
- Reduced to only 4 screws for complete disassembly ( easier and faster to clean after finishing working)
- Industry-graded HDPE pressurized cartridges (200-300cc) safe up to 6.9 bar (normal working pressure is 3-4 bars)
- Custom extruder screws designed in stainless steel and plated to avoid corrosion.
- Silent air compressor ~55db with a rating up to 6bar and 3l deposit with regulating valve and safe release.

![](img/printer.jpg)

### 3D printed version

- all the parts fit in a standard 200x200x200 fdm 3d printer
- almost no support
- max time of file 12hours at 0.3 layer height
- less than 1kg( one spool) of material

### B.O.M

![](img/DSC09088.jpg)

[https://gitlab.com/fablabbcn-projects/paste-extruder/-/blob/main/B.O.M.-PastePrintinPublicVersion%20-%20__2020_21_ClayPrintersKits.pdf](https://gitlab.com/fablabbcn-projects/paste-extruder/-/blob/main/B.O.M.-PastePrintinPublicVersion%20-%20__2020_21_ClayPrintersKits.pdf)

#### Electronics

If you disconnect the fdm print head of the machine you modify you will need to put a 100k ohm resistor in the place where the thermistor of the printhead (temperature sensor) was connected.

### What you will find in this repo.

- 3d models files to print
- some images and visuals of the assembly
- Gcode generators manuals and copy of mamba plugin for grasshopper
- Bill of materials - B.O.M

**Downloads** *You can find all of them in this repo*

### Extras

We recommend you to use a Gcode visualizer before sending the Files to the machine.

We have recompiled this minimalistic version [LINK TO Gcode WebVisualizer](https://fablabbcn-projects.gitlab.io/cnc-machines/g-code-visualizer/)

### Contact

- **eduardo.chamorro@iaac.net**
- **santi@fablabbcn.org**
- **josep@fablabbcn.org**

Thanks

### License

GNU General Public License v3.0 for software (original software credited below). (See LICENSE file).

Cern OHL V1.2 for the Machine Files. (See CERN_OHL file)

### Credits

This design was created for FabLab Barcelona and IAAC(Institute for Advanced Architecture of Catalonia) after 4 years of internal development by Eduardo Chamorro Martin.
Special thanks to:

-Ashkan Foroughi Dehnavi for his heavy work building lots of testing knowledge on its usage
-Josep Marti for aide in testing roughly all versions of it.
-Santiago Fuentemilla, Guillem Camprodon & Alex Dubor for his support on this this project development.
-Daphne Gerodimou for all the huge work on documenting this and its visuals.

##### Disclaimer

<div class="align-justify">
This hardware/software is provided "as is", and you use the hardware/software at your own risk. Under no circumstances shall any author be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use this hardware/software, even if the authors have been advised of the possibility of such damages.</div>
